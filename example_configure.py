#!/bin/env python
import sys
from mkgen import *

'''
    Create new instance of mkgen
'''
mk = Makefile()
mk.cflags.append("-c -Wall -std=c++11")

'''
    Platform dependent flags and libs
'''
if sys.platform == "linux2":
    # linux
    lib = mk.add_library("SDL2")
    lib.cflags.append("$(shell sdl2-config --cflags)")
    lib.lflags.append("$(shell sdl2-config --libs)")
    mk.cflags.append("-fPIC")

if sys.platform == "darwin":
    # macosx
    mk.add_framework("SDL2", "/Library/Frameworks/")
    mk.cflags.append("-fPIC")

'''
    Add include paths
    same as cflags.append("-Isrc/")
'''
mk.add_include("src/")

'''
    Add source files. Second parameter is which part
    of path will be replaced with build directory path.
'''
mk.add_files("src/*.cpp", "src/")

'''
    Add header files.
'''
mk.add_headers("src/*.h")

'''
    Create target instance.
    target is resulting executable or library.
    NOTE: Library support will be added in future.
'''
target = mk.add_target("testapp")
# target should have it's own cflags
target.cflags.append("-O2 -DAPP_DEBUG")
# we need to specify files for linking
target.add_files("src/*.cpp")

'''
    Write final Makefile to file.
'''
mk.build("Makefile")

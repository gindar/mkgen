import os

class DepHunter:
    def __init__(self):
        self.headers_abs = {}
        self.headers_bnm = {}
        self.sources = {}
        self.verbose = False

    def add_headers(self, fnames):
        for pth in fnames:
            bnm = os.path.basename(pth)
            self.headers_abs[pth] = {"deps": []}
            self.headers_bnm[bnm] = pth

    def add_sources(self, fnames):
        for f in fnames:
            self.sources[f] = {"deps": []}

    def add_dep(self, obj, name, level):
        if name not in obj["deps"]:
            obj["deps"].append(name)
            if self.verbose:
                if level >= 0:
                    print("%s * %s" % ("   " * level, name))


    def get_includes(self, src):
        fp = open(src, "rb")
        data = fp.read().split("\n")
        fp.close()
        result = []

        index = 0
        for line in data:
            if "#include" in line:
                bname = line.replace("#include ", "").replace("\"", "").replace("\'", "").replace(" ", "")
                result.append(bname)
            index += 1
            if index > 100:
                break
        return result

    def hunt(self):

        for f in self.headers_abs:
            self._huntHeader(f, self.headers_abs[f])

        for f in self.sources:
            self._hunt(f, self.sources[f])


    def _huntHeader(self, src, obj):
        incs = self.get_includes(src)
        for bname in incs:
            if bname in self.headers_bnm:
                aname = self.headers_bnm[bname]
                self.add_dep(obj, aname, -1)

    def _hunt(self, src, obj):
        if self.verbose:
            print("%s depends on:" % src)
        incs = self.get_includes(src)
        for bname in incs:
            if bname in self.headers_bnm:
                aname = self.headers_bnm[bname]
                self.add_dep(obj, aname, 1)
                self.collect_deps(obj, aname, 2)


    def collect_deps(self, obj, aname, level=0):
        hdeps = self.headers_abs[aname]["deps"]
        for h in hdeps:
            self.add_dep(obj, h, level)
            self.collect_deps(obj, h, level + 1)

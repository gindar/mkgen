# mkgen

Makefile generator written in python. It's lightweight and simple cmake-like script. 
Generated Makefile is watching .h files changes and supports multiple targets.

## Installation

 * Download and install python 2.7 http://python.org
 * checkout or download mkgen
 * go to mkgen directory and type: `python setup.py install`

## Usage

See `example_configure.py` file.
